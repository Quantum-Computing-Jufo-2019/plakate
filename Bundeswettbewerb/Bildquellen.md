# Bildquellen Plakate

- Plakat: Lösung des n-Damenproblems auf einem adiabatischen Quantencomputer
  - Abb. „D-Wave Quantencomputer“: https://www.dwavesys.com/sites/default/files/2000Q%20Extracted%20Image%20website.jpeg
- Plakat: Funktionsweise eines D-Wave Quantum Annealers
  - Abb. „Chimera-Graph mit 7 logischen Qubits“: https://www.semanticscholar.org/paper/Fast-clique-minor-generation-in-Chimera-qubit-Boothby-King/39130a8ff551498da753acb0d9a3787f3f403b11/figure/1
  - Abb. „D-Wave Leap Portal“: https://cloud.dwavesys.com/leap/
  - Abbildungen „Quantum Processing unit“: https://www.dwavesys.com/resources/media-resources
  - Abb. 1: https://www.dwavesys.com/sites/default/files/tut-hardware-qubit-schematic.jpg
  - Abb. 2: https://www.dwavesys.com/sites/default/files/tut-hardware-qubit-loops.jpg
  - Abb. 5: https://docs.dwavesys.com/docs/latest/_downloads/09-1076A-T_GettingStarted.pdf (S. 11)
